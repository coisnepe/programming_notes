```js
const constVariable
let letVariable
var varVariable

// 09 - Default Function Arguments
  // put default arguments and pass undefined as argument
  function becomeBetter(goal, average = 50, effort = 200);
  becomeBetter(100, undefined, 50);
```

```js
// Refactor functions to arrow functions
const names = ['wes', 'kait', 'lux'];

const fullNames = names.map(function(name) {
  return `${name} bos`;
});

const fullNames2 = names.map((name) => {
  return `${name} bos`;
});

const fullNames3 = names.map(name => {
  return `${name} bos`;
});

const fullNames4 = names.map(name => `${name} bos`);

const fullNames5 = names.map(() => `cool bos`);

const sayMyName = (name) => {
  alert(`Hello ${name}!`)
}

sayMyName('Wes');
```
