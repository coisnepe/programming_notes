```js
const ages = [1, 234, 35, 34, 145, 453];

const filteredAges = ages.filter(age => age >= 60);
```

```js
// document
  // querySelector
    document.querySelector(".desiredClass");
    document.querySelector("div.firstClass input[name=inputname]");
```

```js
// element
  // classList
    element.classList.add("another", "multipleclasses");
    element.classList.remove("onelast");
    element.classList.toggle("toggled");
```

```js
// Node (an )
  // contains
    Node.contains("string");
```

```js
element.addEventListener('click', function(){});

setTimeout(function(){}, 1000);

element.classList.
```
